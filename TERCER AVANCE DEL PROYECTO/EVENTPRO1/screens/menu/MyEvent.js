import React, { useState, useEffect, useRef } from 'react';
import { View, Text, TouchableOpacity, FlatList, StyleSheet, Animated, Easing } from 'react-native';
import { useNavigation, DrawerActions } from '@react-navigation/native';
import DrawerSceneWrapper from '../../components/menu/DrawerSceneWrapper';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import API_URL from '../../apiConfig'; 

const CLIENT_ID = 43; 


const estadoDescripcionMap = {
  'PRO': 'Event in process',
  'INI': 'Event started',
  'TER': 'Event completed',
};

const MyEvent = () => {
  const navigation = useNavigation();
  const [events, setEvents] = useState([]);
  const [clientName, setClientName] = useState('');

  useEffect(() => {
    fetchClientEvents();
    fetchClientName(); 

    const interval = setInterval(() => {
      fetchClientEvents();
      fetchClientName(); 
    }, 5000); 

    return () => clearInterval(interval);
  }, []);

  const fetchClientEvents = async () => {
    try {
      const response = await fetch(`${API_URL}/cliente/${CLIENT_ID}/evento`);
      if (!response.ok) {
        throw new Error('Error fetching client events');
      }
      const clientEvents = await response.json();
      setEvents(clientEvents);
    } catch (error) {
      console.error('Error fetching client events:', error);
    }
  };

  const fetchClientName = async () => {
    try {
      const response = await fetch(`${API_URL}/cliente/${CLIENT_ID}`);
      if (!response.ok) {
        throw new Error('Error fetching client name');
      }
      const clientData = await response.json();
      setClientName(clientData.nombre);
    } catch (error) {
      console.error('Error fetching client name:', error);
    }
  };

  const handleGoBackToDrawer = () => {
    navigation.dispatch(DrawerActions.openDrawer());
  };

  const slideValue = useRef(new Animated.Value(-100)).current;
  const fadeInValue = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    Animated.parallel([
      Animated.timing(
        slideValue,
        {
          toValue: 0,
          duration: 1000,
          useNativeDriver: true,
          easing: Easing.out(Easing.back()),
        }
      ),
      Animated.timing(
        fadeInValue,
        {
          toValue: 1,
          duration: 1000,
          useNativeDriver: true,
        }
      )
    ]).start();
  }, []);

  // Función para obtener la descripción del estado
  const obtenerDescripcionEstado = (codigoEstado, fechaEvento) => {
    const hoy = new Date();
    const fechaEventoDate = new Date(fechaEvento);
    if (fechaEventoDate > hoy) {
      return 'Event in process';
    } else if (fechaEventoDate.getDate() === hoy.getDate()) {
      return 'Event started';
    } else {
      return 'Evento terminado';
    }
  };

  return (
    <DrawerSceneWrapper>
       <TouchableOpacity onPress={handleGoBackToDrawer} style={styles.menuButton}>
          <MaterialCommunityIcons name="menu" size={26} color="#009688" />
        </TouchableOpacity>
      <View style={styles.bottomSpace}></View>
      <View style={{ flex: 1, backgroundColor: 'white', paddingHorizontal: 20 }}>
        <Animated.Text style={[styles.headerText, { opacity: fadeInValue, transform: [{ translateY: slideValue }] }]}>
          My Events for : {clientName}
        </Animated.Text>
        <View style={styles.tableContainer}>
          <View style={styles.tableHeader}>
            <Text style={styles.headerCell}>Event Name</Text>
            <Text style={styles.headerCell}>Event Date</Text>
            <Text style={styles.headerCell}>Event State</Text> 
          </View>
          <FlatList
            data={events}
            keyExtractor={(item) => item.numero.toString()}
            renderItem={({ item, index }) => (
              <Animated.View style={[styles.tableRow, { opacity: fadeInValue, transform: [{ translateY: slideValue }] }]}>
                <Text style={styles.cell}>{item.nombre_evento}</Text>
                <Text style={styles.cell}>{item.fecha_evento.split('T')[0]}</Text>
                <Text style={styles.cell}>{obtenerDescripcionEstado(item.estado, item.fecha_evento)}</Text> 
              </Animated.View>
            )}
          />
        </View>
      </View>
    </DrawerSceneWrapper>
  );
};

const styles = StyleSheet.create({
  menuButton: {
    position: 'absolute',
    top: 30,
    left: 20,
    zIndex: 999,
  },
  headerText: {
    fontSize: 24,
    marginBottom: 10,
    marginTop: 10,
    textAlign: 'center',
    color: '#009688', // Color aqua
    fontWeight: 'bold',
  },
  tableContainer: {
    flex: 1,
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 5,
    overflow: 'hidden',
    marginTop: 10,
  },
  tableHeader: {
    flexDirection: 'row',
    backgroundColor: '#f2f2f2',
    paddingVertical: 10,
    paddingHorizontal: 5,
  },
  headerCell: {
    flex: 1,
    fontWeight: 'bold',
  },
  tableRow: {
    flexDirection: 'row',
    paddingVertical: 5,
    paddingHorizontal: 5,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
  },
  cell: {
    flex: 1,
  },
  bottomSpace: {
    paddingBottom: 80,
    backgroundColor:'white', // Additional space at the end of the content
  },
});

export default MyEvent;
