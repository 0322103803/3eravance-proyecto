import React, { useState, useEffect } from 'react';
import { StyleSheet, View, TouchableOpacity, Text, Image, Animated, Easing } from 'react-native';
import MapView, { Marker } from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import * as Location from 'expo-location';
import { useNavigation } from '@react-navigation/native';
import { HOTELS } from '../../data/index';

const GOOGLE_MAPS_KEY = 'AIzaSyBoKxAB5IT_N4iTrw0Ykvpl-OT9rRF3YSA';

export default function MapScreen({ route }) {
  const navigation = useNavigation();
  const [origin, setOrigin] = useState(null);
  const [destination, setDestination] = useState(null);
  const animationValue = new Animated.Value(0);

  useEffect(() => {
    getLocation();
    animateMarker();
  }, []);

  const getLocation = async () => {
    let { status } = await Location.requestForegroundPermissionsAsync();
    if (status !== 'granted') {
      alert('Permission denied');
      return;
    }
    let location = await Location.getCurrentPositionAsync({});
    const current = {
      latitude: location.coords.latitude,
      longitude: location.coords.longitude,
    };
    setOrigin(current);
  };

  useEffect(() => {
    if (route.params && route.params.hotelId) {
      setDestination(HOTELS[route.params.hotelId]);
    }
  }, [route.params]);

  const animateMarker = () => {
    Animated.timing(animationValue, {
      toValue: 1,
      duration: 2000,
      easing: Easing.linear,
      useNativeDriver: true,
    }).start(() => {
      animationValue.setValue(0);
      animateMarker();
    });
  };

  const markerStyle = {
    transform: [
      {
        translateY: animationValue.interpolate({
          inputRange: [0, 1],
          outputRange: [0, 10],
        }),
      },
    ],
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.backButton} onPress={() => navigation.goBack()}>
        <Icon name="arrow-left" size={24} color="black" />
      </TouchableOpacity>
      {origin && (
        <MapView
          style={styles.map}
          initialRegion={{
            latitude: origin.latitude,
            longitude: origin.longitude,
            latitudeDelta: 0.09,
            longitudeDelta: 0.04,
          }}
        >
          {origin && (
            <Marker coordinate={origin}>
              <Animated.Image
                source={require('../../assets/images/car.png')}
                style={[styles.customMarker, markerStyle]}
              />
            </Marker>
          )}
          {Object.values(HOTELS).map((hotel) => (
            <Marker
              key={hotel.id}
              coordinate={{ latitude: hotel.latitude, longitude: hotel.longitude }}
              title={hotel.title}
              onPress={() => setDestination(hotel)}
            >
              <View style={styles.markerContainer}>
                <Image source={hotel.image} style={styles.markerImage} />
              </View>
            </Marker>
          ))}
          {destination && (
            <Marker
              coordinate={{ latitude: destination.latitude, longitude: destination.longitude }}
              pinColor="red"
            />
          )}
          {destination && (
            <MapViewDirections
              origin={origin}
              destination={destination}
              apikey={GOOGLE_MAPS_KEY}
              strokeColor="white"
              strokeWidth={5}
            />
          )}
        </MapView>
      )}
      {destination && (
        <View style={styles.hotelInfo}>
          <Text>{destination.title}</Text>
          <Image source={destination.image} style={styles.hotelImage} />
        </View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f0f0f0',
  },
  map: {
    flex: 1,
  },
  backButton: {
    position: 'absolute',
    top: 40,
    left: 20,
    zIndex: 10,
    backgroundColor: 'rgba(255, 255, 255, 0.7)',
    borderRadius: 20,
    padding: 10,
  },
  markerContainer: {
    width: 40,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  markerImage: {
    width: 30,
    height: 30,
    resizeMode: 'cover',
    borderRadius: 15,
  },
  customMarker: {
    width: 15,
    height: 40,
    resizeMode: 'cover',
  },
  hotelInfo: {
    position: 'absolute',
    bottom: 20,
    left: 20,
    zIndex: 10,
    backgroundColor: 'rgba(255, 255, 255, 0.7)',
    padding: 10,
    borderRadius: 10,
  },
  hotelImage: {
    width: 100,
    height: 100,
    resizeMode: 'cover',
    borderRadius: 10,
    marginTop: 5,
  },
});