import React, { useEffect, useRef } from 'react';
import { StyleSheet, Animated, TouchableOpacity, Text, View, Easing } from 'react-native';
import Material from 'react-native-vector-icons/MaterialCommunityIcons';

const TabButton = ({ item, accessibilityState, onPress }) => {
    const animatedValues = {
        translateY: useRef(new Animated.Value(0)).current,
        scale: useRef(new Animated.Value(0)).current,
    };

    const { translateY, scale } = animatedValues;

    useEffect(() => {
        handleAnimated();
    }, [accessibilityState.selected]);

    const handleAnimated = () => {
        const durationTranslate = accessibilityState.selected ? 400 : 300; // Duración de la animación de desplazamiento
        const durationScale = accessibilityState.selected ? 300 : 200; // Duración de la animación de escala

        Animated.parallel([
            Animated.timing(translateY, {
                toValue: accessibilityState.selected ? -30 : 0,
                duration: durationTranslate,
                useNativeDriver: true,
                easing: Easing.out(Easing.ease) // Curva de la animación de desplazamiento
            }),
            Animated.timing(scale, {
                toValue: accessibilityState.selected ? 1 : 0,
                duration: durationScale,
                useNativeDriver: true,
                easing: Easing.out(Easing.ease) // Curva de la animación de escala
            })
        ]).start();
    };

    const scaleStyles = {
        opacity: scale.interpolate({
            inputRange: [0, 1],
            outputRange: [0.5, 1], // Ajuste de opacidad al cambiar la escala
            extrapolate: 'clamp'
        }),
        transform: [
            {
                scale: scale
            }
        ]
    };

    return (
        <View style={styles.tabContainer}>
            <TouchableOpacity onPress={onPress} style={styles.container}>
                <Animated.View style={[styles.button, { transform: [{ translateY }] }, { alignItems: 'center' }]}>
                    <Animated.View style={[styles.circle, scaleStyles]} />
                    <Material name={item.icon} color={accessibilityState.selected ? '#fff' : '#009688'} size={22} />
                </Animated.View>
                <Animated.Text style={[styles.title, { opacity: scale }]}>{item.title}</Animated.Text>
            </TouchableOpacity>
        </View>
    );
};

const styles = StyleSheet.create({
    tabContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 70,
        alignSelf: 'stretch'
    },
    button: {
        width: 50,
        height: 50,
        borderRadius: 25,
        borderWidth: 4,
        borderColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        overflow: 'hidden'
    },
    circle: {
        width: 50,
        height: 50,
        borderRadius: 25,
        position: 'absolute',
        backgroundColor: '#009688',
    },
    title: {
        fontSize: 10,
        fontWeight: 'bold',
        textAlign: 'center',
        color: '#009688',
        position: 'absolute',
        bottom: 20,
    },
});

export default TabButton;

